package com.niemar.searchplace.controller;

import com.niemar.searchplace.dto.SearchPlaceResponse;
import com.niemar.searchplace.service.SearchPlaceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class SearchPlaceController {

    private SearchPlaceService searchPlaceService;

    public SearchPlaceController(SearchPlaceService searchPlaceService) {
        this.searchPlaceService = searchPlaceService;
    }

    @GetMapping(value = "{country}/{city}/{query}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SearchPlaceResponse> search(@PathVariable String country, @PathVariable String city, @PathVariable String query) {
        SearchPlaceResponse response = searchPlaceService.search(country, city, query);
        if (response.getErrorMessage() != null) {
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(response);
    }
}