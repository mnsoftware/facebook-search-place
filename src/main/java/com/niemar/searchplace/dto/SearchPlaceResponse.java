package com.niemar.searchplace.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class SearchPlaceResponse {

    private final List<PlaceResponse> places;

    private final String errorMessage;

    public SearchPlaceResponse(List<PlaceResponse> places) {
        this(places, null);
    }

    public SearchPlaceResponse(String errorMessage) {
        this(Collections.emptyList(), errorMessage);
    }

    private SearchPlaceResponse(List<PlaceResponse> places, String errorMessage) {
        this.places = places;
        this.errorMessage = errorMessage;
    }


    public List<PlaceResponse> getPlaces() {
        return places;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchPlaceResponse)) return false;
        SearchPlaceResponse that = (SearchPlaceResponse) o;
        return Objects.equals(places, that.places) &&
                Objects.equals(errorMessage, that.errorMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(places, errorMessage);
    }

    @Override
    public String toString() {
        return "SearchPlaceResponse{" +
                "places=" + places +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}