package com.niemar.searchplace.service;

import com.niemar.searchplace.dto.PlaceResponse;
import com.niemar.searchplace.dto.SearchPlaceResponse;
import com.restfb.Connection;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.exception.FacebookNetworkException;
import com.restfb.types.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class SearchPlaceService {

    private final FacebookClient fbClient;

    @Autowired
    public SearchPlaceService(FacebookClient fbClient) {
        this.fbClient = fbClient;
    }

    public SearchPlaceResponse search(String country, String city, String query) {
        if (!validateParams(country, city, query)) {
            return new SearchPlaceResponse("Couldn't find location because one of the parameters [country, city, query] is empty !");
        }

        // merge params to make just one call to fb api, this is efficient and provide good results
        String q = createQuery(country, city, query);
        List<Place> searchResults;
        try {
            searchResults = fetchPlaces(q);
        } catch (FacebookNetworkException e) {
            return new SearchPlaceResponse("Not found, please see details: " + e.getMessage());
        }

        /*
            Filtering results by city and country.
            It is possible that in response will be places from cities with
            the same names but as soon as longitude and latitude are different this is no problem.
         */
        List<PlaceResponse> places = searchResults.stream()
                .filter(locationExist())
                .filter(filterByCity(city))
                .filter(filterByCountry(country))
                .map(mapToPlaceResponse())
                .collect(Collectors.toList());
        return new SearchPlaceResponse(places);
    }

    private Predicate<Place> filterByCountry(String country) {
        return place -> place.getLocation().getCountry().equalsIgnoreCase(country);
    }

    private Predicate<Place> locationExist() {
        return place -> place.getLocation() != null;
    }

    private Predicate<Place> filterByCity(String city) {
        return place -> place.getLocation().getCity().equalsIgnoreCase(city);
    }

    private List<Place> fetchPlaces(String q) {
        Connection<Place> lists = fbClient.fetchConnection("search", Place.class,
                Parameter.with("q", q),
                Parameter.with("type", "place"),
                Parameter.with("fields", "name,location"));
        return lists.getData();
    }

    private boolean validateParams(String country, String city, String query) {
        return !StringUtils.isEmpty(country) && !StringUtils.isEmpty(city) && !StringUtils.isEmpty(query);
    }

    private Function<Place, PlaceResponse> mapToPlaceResponse() {
        return place -> new PlaceResponse(place.getName(), place.getLocation().getLatitude(), place.getLocation().getLongitude());
    }

    private String createQuery(String country, String city, String query) {
        return country + " " + city + " " + query;
    }
}