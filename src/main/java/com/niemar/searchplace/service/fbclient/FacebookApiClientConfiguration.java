package com.niemar.searchplace.service.fbclient;

import com.restfb.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FacebookApiClientConfiguration {

    // @TODO Should be in safe place, not here !
    private static final String ACCESS_TOKEN = "EAACyeZApkKIEBAGLDhgRZBi3YHARbUxk5NZCGChwvoMahrT11zy1hLhF59621ZANLFkms05kGrMGPeObPZBI3z5fZCbCDlm8uXnTYOwitVTYC4auzgux3aNYP8082p0Y00KXphMsZAI0YRw15xHoFZCOCqN4gRIDtxevoGr8ibiLZCQZDZD";

    @Bean
    public FacebookClient createClient() {
        // TODO couldn't configure client to use http client, maybe RestTemplate would be better choice ?
        return new DefaultFacebookClient(ACCESS_TOKEN, createWebRequestor(),
                new DefaultJsonMapper(), Version.LATEST);
    }

    @Bean
    protected WebRequestor createWebRequestor() {
        return new ProductionWebRequestor();
    }
}