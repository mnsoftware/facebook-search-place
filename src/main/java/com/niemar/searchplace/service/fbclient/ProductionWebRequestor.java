package com.niemar.searchplace.service.fbclient;

import com.restfb.DefaultWebRequestor;

import java.net.HttpURLConnection;

public class ProductionWebRequestor extends DefaultWebRequestor {

    //TODO how to put to config file using @Value and read in wiremock tests ?
    private static int FB_CLIENT_READ_TIMEOUT_MILIS = 3000;

    private static int FB_CLIENT_CONNECTION_TIMEOUT_MILIS = 5000;

    protected void customizeConnection(HttpURLConnection connection) {
        connection.setReadTimeout(FB_CLIENT_READ_TIMEOUT_MILIS);
        connection.setConnectTimeout(FB_CLIENT_CONNECTION_TIMEOUT_MILIS);
    }

}
