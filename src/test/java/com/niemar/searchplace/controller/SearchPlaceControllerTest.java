package com.niemar.searchplace.controller;

import com.niemar.searchplace.dto.PlaceResponse;
import com.niemar.searchplace.dto.SearchPlaceResponse;
import com.niemar.searchplace.service.SearchPlaceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SearchPlaceController.class)
public class SearchPlaceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchPlaceService service;

    @Test
    public void searchTest() throws Exception {
        SearchPlaceResponse response = createResponse();
        given(service.search("Poland", "Poznan", "Egnyte")).willReturn(response);

        mockMvc.perform(get("/Poland/Poznan/Egnyte").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.places[0].name", is(response.getPlaces().get(0).getName())))
                .andExpect(jsonPath("$.places[0].longitude", is(response.getPlaces().get(0).getLongitude())))
                .andExpect(jsonPath("$.places[0].latitude", is(response.getPlaces().get(0).getLatitude())));
    }

    @Test
    public void errorTest() throws Exception {
        SearchPlaceResponse response = new SearchPlaceResponse("Not found: internal Error");
        given(service.search("Poland", "Poznan", "Egnyte")).willReturn(response);

        mockMvc.perform(get("/Poland/Poznan/Egnyte").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(404))
                .andExpect(jsonPath("$.errorMessage", is(response.getErrorMessage())));
    }

    @Test
    public void unsupportedContentTypeTest() throws Exception {
        SearchPlaceResponse response = new SearchPlaceResponse("Not found: internal Error");
        given(service.search("Poland", "Poznan", "Egnyte")).willReturn(response);

        mockMvc.perform(get("/Poland/Poznan/Egnyte").accept(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isNotAcceptable());

    }

    private SearchPlaceResponse createResponse() {
        List<PlaceResponse> places = List.of(
                new PlaceResponse("Egnyte Poland", 52.404719116912, 16.940510409764)
        );
        return new SearchPlaceResponse(places);
    }
}