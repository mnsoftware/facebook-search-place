package com.niemar.searchplace.integeration;

import com.niemar.searchplace.Application;
import com.niemar.searchplace.dto.PlaceResponse;
import com.niemar.searchplace.dto.SearchPlaceResponse;
import com.niemar.searchplace.service.SearchPlaceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * End to end test, it may be unstable since they depend on facebook api server state.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@AutoConfigureMockMvc
public class SearchPlaceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SearchPlaceService service;

    @Test
    public void searchTest() throws Exception {
        SearchPlaceResponse response = createResponse();
        mockMvc.perform(get("/Poland/Poznan/Egnyte").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.places[0].name", is(response.getPlaces().get(0).getName())))
                .andExpect(jsonPath("$.places[0].longitude", is(response.getPlaces().get(0).getLongitude())))
                .andExpect(jsonPath("$.places[0].latitude", is(response.getPlaces().get(0).getLatitude())));
    }

    private SearchPlaceResponse createResponse() {
        List<PlaceResponse> places = List.of(
                new PlaceResponse("Egnyte Poland", 52.404719116912, 16.940510409764)
        );
        return new SearchPlaceResponse(places);
    }
}
