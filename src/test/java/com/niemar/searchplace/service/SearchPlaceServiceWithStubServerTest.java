package com.niemar.searchplace.service;

import com.github.tomakehurst.wiremock.http.Fault;
import com.niemar.searchplace.dto.PlaceResponse;
import com.niemar.searchplace.dto.SearchPlaceResponse;
import com.niemar.searchplace.service.fbclient.ProductionWebRequestor;
import com.restfb.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
/**
 * Tests with stub server Wiremock, helpful to test timeouts and other server problems.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 8087)
@TestPropertySource(properties = {"facebook.api.client.read.timeout.milis=3000", "facebook.api.client.connection.timeout.milis=5000"})
public class SearchPlaceServiceWithStubServerTest {

    // @TODO Should be in safe place, not here !
    private static final String ACCESS_TOKEN = "EAACyeZApkKIEBAGLDhgRZBi3YHARbUxk5NZCGChwvoMahrT11zy1hLhF59621ZANLFkms05kGrMGPeObPZBI3z5fZCbCDlm8uXnTYOwitVTYC4auzgux3aNYP8082p0Y00KXphMsZAI0YRw15xHoFZCOCqN4gRIDtxevoGr8ibiLZCQZDZD";

    @TestConfiguration
    static class FacebookClientTestContextConfiguration {

        @Bean
        public FacebookClient createClient() {
            DefaultFacebookClient fbClient = new DefaultFacebookClient(ACCESS_TOKEN, new ProductionWebRequestor(),
                    new DefaultJsonMapper(), Version.LATEST);
            fbClient.setFacebookEndpointUrls(createTestEndpoints());
            return fbClient;
        }

        @Bean
        public FacebookEndpoints createTestEndpoints() {
            return new FacebookEndpoints() {
                @Override
                public String getFacebookEndpoint() {
                    return null;
                }

                @Override
                public String getGraphEndpoint() {
                    return "http://localhost:8087";
                }

                @Override
                public String getGraphVideoEndpoint() {
                    return null;
                }
            };
        }
    }

    @Autowired
    private SearchPlaceService service;

    @Test
    public void searchTest() {
        SearchPlaceResponse expected = createExpectedResponse();
        stubFor(get(urlPathEqualTo("/v3.0/search"))
                .withQueryParam("q", equalTo("Poland Warsaw Ikea"))
                .withQueryParam("type", equalTo("place"))
                .withQueryParam("fields", equalTo("name,location"))
                .withQueryParam("access_token", equalTo(ACCESS_TOKEN))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("/okResponse.json")));

        SearchPlaceResponse searchPlaceResponse = service.search("Poland", "Warsaw", "Ikea");

        Assert.assertEquals(expected, searchPlaceResponse);
    }

    @Test
    public void timeoutTest() {
        SearchPlaceResponse expected = new SearchPlaceResponse("Not found, please see details:" +
                " A network error occurred while trying to communicate with Facebook: Facebook request failed (HTTP status code null)");
        stubFor(get(urlPathEqualTo("/v3.0/search"))
                .withQueryParam("q", equalTo("Poland Warsaw Ikea"))
                .withQueryParam("type", equalTo("place"))
                .withQueryParam("fields", equalTo("name,location"))
                .withQueryParam("access_token", equalTo(ACCESS_TOKEN))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("/okResponse.json")
                        .withFixedDelay(10000)));

        SearchPlaceResponse searchPlaceResponse = service.search("Poland", "Warsaw", "Ikea");

        Assert.assertEquals(expected, searchPlaceResponse);
    }

    @Test
    public void testMalformedResponse() {
        SearchPlaceResponse expected = new SearchPlaceResponse("Not found, please see details:" +
                " A network error occurred while trying to communicate with Facebook: Facebook request failed (HTTP status code null)");
        stubFor(get(urlPathEqualTo("/v3.0/search"))
                .withQueryParam("q", equalTo("Poland Warsaw Ikea"))
                .withQueryParam("type", equalTo("place"))
                .withQueryParam("fields", equalTo("name,location"))
                .withQueryParam("access_token", equalTo(ACCESS_TOKEN))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("/okResponse.json")
                        .withFault(Fault.MALFORMED_RESPONSE_CHUNK)));

        SearchPlaceResponse searchPlaceResponse = service.search("Poland", "Warsaw", "Ikea");

        Assert.assertEquals(expected, searchPlaceResponse);
    }

    private SearchPlaceResponse createExpectedResponse() {
        List<PlaceResponse> places = List.of(
                new PlaceResponse("IKEA", 52.30521, 21.08336),
                new PlaceResponse("IKEA", 52.133329985709, 20.894794464111),
                new PlaceResponse("Kuchnia Spotkań IKEA", 52.22853735356, 21.0055496321),
                new PlaceResponse("Centrum IKEA dla Firm", 52.22811, 21.01227),
                new PlaceResponse("Inna Bajka", 52.135369515732, 20.897992339461)
        );
        return new SearchPlaceResponse(places);
    }
}